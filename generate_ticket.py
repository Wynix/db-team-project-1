import sys
import random

def generate_sql(seatnumber, enumber, cnumber, stnumber):
    sql_string = "CREATE TEMPORARY TABLE TMP AS ( "
    sql_string += "WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( "
    sql_string += str(seatnumber) + ", " + str(10000) + ", '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );\n\n"

    sql_string += "WITH T AS ( select (mpnumber) from PLAN where stnumber = " + stnumber \
                  + " ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP))\n\n";

    
    sql_string += "INSERT INTO PRINT (enumber, tnumber)\n"
    sql_string += "VALUES (" + str(enumber) \
                  + ", (SELECT TMP.tnumber FROM TMP));\n\n"
    
    sql_string += "INSERT INTO INFORMATION (tnumber, mpnumber)\n"
    sql_string += "VALUES ((SELECT TMP.tnumber FROM TMP), " \
                  + str(stnumber) + ");\n\n"

    sql_string += "INSERT INTO RESERVATION (cnumber, tnumber, stnumber)\n"
    sql_string += "VALUES (" + str(cnumber) \
                  + ", (SELECT TMP.tnumber FROM TMP), " \
                  + stnumber + ");\n\n"

    sql_string += "DROP TABLE TMP;\n\n"
    
    print(sql_string)


if len(sys.argv) < 3:
    sys.stderr.write("Usage : python3 generate_ticket.py <start customer> <end customer> <primary key of screening table> \n")
else:
    print("----")
    start  = int(sys.argv[1])
    diff = int(sys.argv[2]) - int(sys.argv[1])
    for i in range(0, diff+1):
        generate_sql(i, random.randint(1,3), start+1, sys.argv[3])
