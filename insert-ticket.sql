-- 
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET (seatnumber, tprice, discount, pmethod, age) VALUES (1, 10000, '할인미적용', '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 1);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;

--
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET (seatnumber, tprice, discount, pmethod, age) VALUES (2, 10000, '할인미적용', '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 1);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;
----
CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 0, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 1, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 2, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 3, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 4, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 5, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (3, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 6, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 7, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 8, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 9, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 10, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (1, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


CREATE TEMPORARY TABLE TMP AS ( WITH T AS ( INSERT INTO TICKET ( seatnumber, tprice, discount, pmethod, age) VALUES ( 11, 10000, '할인미적용' , '카드', '성인') RETURNING tnumber ) SELECT T.tnumber FROM T );

WITH T AS ( select (mpnumber) from PLAN where stnumber = 4 ) INSERT INTO INFORMATION (mpnumber, tnumber) VALUES ((SELECT T.mpnumber FROM T), (SELECT TMP.tnumber FROM TMP));

INSERT INTO PRINT (enumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

INSERT INTO INFORMATION (tnumber, mpnumber)
VALUES ((SELECT TMP.tnumber FROM TMP), 4);

INSERT INTO RESERVATION (cnumber, tnumber)
VALUES (2, (SELECT TMP.tnumber FROM TMP));

DROP TABLE TMP;


