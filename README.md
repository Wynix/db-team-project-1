# 사용법

* 데이터 베이스를 초기화 후 데이터 삽입
```bash
$ psql < complete.sql
```

* 테이블의 스키마 확인
```sql
)> \dt CUSTOMER;
```

* 테이블 조회
```sql
)> SELECT * FROM CUSTOMER;
```

# SQL 파일 설명

* `create-table.sql` : 테이블을 생성하는 쿼리문
* 테스트 목적으로 데이터를 삽입하기 위한 쿼리문
    * `insert-default-data.sql` : 변동이 그나마 적은 영화, 판매원, 고객에 대한 정보를 삽입하는 쿼리문
    * `insert-screening-table.sql` : 상영 시간표에 대한 정보를 삽입하는 쿼리문
    * `insert-ticket.sql` : 티켓 발급에 대한 정보를 삽입하는 쿼리문
* `complete.sql` : 모든 SQL 쿼리문을 합친 결과물
