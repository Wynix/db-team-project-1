-- * 특정 테이블의 특정 컬럼만 조회하는 select 문 3개
-- 1. 영화 테이블에서 영화 제목과 장르를 조회하는 select문
SELECT mtitle, mgenre
FROM movie;
-- 2. 전체 고객(회원과 비회원 포함)의 이름과 나이, 연락처를 조회
SELECT cname , age, phonenumber
FROM customer;
-- 3. 연령 구분에 따른(성인, 청소년) 티켓 구매 시 카드, 현금 결제 성향 조회
SELECT age, pmethod
FROM ticket;
-- * 특정 테이블의 특정 조건의 특정 컬럼만 조회하는 select문 3개
-- 1. 전체 영화 중에서 영화 런타임이 100분을 넘지 않는 영화들과 그 영화들의 런타임 조회
SELECT mtitle, mruntime
FROM movie
WHERE mruntime < 100;
-- 2. 전체 고객(회원과 비회원 포함)중 미성년자인 고객들의 이름과 나이 조회
SELECT cname, age
FROM customer
WHERE age < 20;
--  3. 전체 티켓 구매자 중 현금 결제를 하여 티켓을 구매한 현황 조회
SELECT *
FROM ticket
WHERE pmethod ='현금';



-- 특정 조건(where)에 대한 update 문 1개
-- 비밀번호를 잊어 초기화를 원하는 손님의 비밀번호 초기화
-- Ex) 비밀번호를 잊은 손님의 이름은 도찬호 이며 전화번호는 010-5633-3343 이라고 한다.
UPDATE customer SET cpw='a0123456789'
WHERE cname='도찬호' AND phonenumber='010-5633-3343';


-- * Join을 이용한 Select문 3개

-- 1. 특정 영화의 상영시간 목록

SELECT (M.mtitle, ST.period, ST.starttime)
FROM PLAN PP, MOVIE M, SCREENING_TABLE ST
WHERE M.mtitle = '킬빌' AND PP.mpnumber = M.mpnumber AND PP.stnumber = ST.stnumber;

-- 2. POS기에서 현금 결제한 티켓 목록
SELECT P.ename, T.*
FROM (
     SELECT PP.tnumber, E.ename 
     FROM ( SELECT EE.enumber, EE.ename FROM EMPLOYEE EE WHERE EE.ename LIKE 'POS%' ) AS E, PRINT PP WHERE PP.enumber = E.enumber
     ) AS P, TICKET T
WHERE T.tnumber = P.tnumber;

-- 3. 특정 고객이 본 영화 목록
SELECT *
FROM
  MOVIE M,
  (
    SELECT DISTINCT SST.mpnumber
    FROM
      SCREENING_TABLE AS SST,
      (
        SELECT RR.stnumber
        FROM
          ( SELECT cnumber FROM CUSTOMER WHERE cname = '김정걸') AS C,
	  RESERVATION RR
        WHERE RR.cnumber = C.cnumber
      ) AS R
    WHERE SST.stnumber = R.stnumber
  ) AS ST
WHERE mpnumber = ST.mpnumber;


-- * 집계함수와 Group by, having, order by를 이용한 select문 3개
-- 1. 오후시간대의특정영화를관람한고객수
SELECT ST.mtitle, COUNT(*)
FROM
  RESERVATION R,
  (SELECT SST.stnumber, P.mtitle
   FROM
     (SELECT (PP.mpnumber, M.mtitle)
      FROM MOVIE M, PLAN PP
      WHERE M.mpnumber = PP.mpnumber
     ) AS P,
     SCREENING_TABLE SST
   WHERE SST.stnumber = P.stnumber
  ) AS ST
GROUP BY ST.starttime
HAVING ST.starttime > '12:00';


-- 2. 시간대별영화예매횟수
SELECT COUNT(*)
FROM
  (SELECT SST.* 
   FROM RESERVATION R, SCREENING_TABLE SST
   WHERE R.stnumber = SST.stnumber) AS ST
GROUP BY starttime
ORDER BY starttime ASC;

-- 3. 연령별예매횟수
SELECT (C.age, COUNT(*))
FROM
  CUSTOMER C, RESERVATION R
WHERE C.cnumber = R.cnumber
GROUP BY C.age
ORDER BY C.age ASC;


-- * Subquery를 이용한 Select문 3개
-- 1. 총예매금액이일정금액이상인고객의목록


SELECT (C.cname, SUM(T.tprice))
FROM
  ( SELECT R.cnumber, TT.tprice
    FROM RESERVATION R, TICKET TT
    WHERE TT.tnumber = R.tnumber
  ) AS T, CUSTOMER C
WHERE C.cnumber = T.cnumber
GROUP BY C.cnumber
HAVING SUM(T.tprice) > 20000
ORDER BY SUM(T.tprice) DESC;


-- 2. 특정영화의멤버십등급별예매횟수
SELECT (M.mtitle, ST.rank, COUNT(*))
FROM
  ( SELECT C.cnumber, R.stnumber, C.crank
    FROM
      ( SELECT CC.cnumber, M.crank
        FROM CUSTOMER CC, MEMBER M
        WHERE CC.cnumber = M.cnumber) AS C,
      RESERVATION R
    WHERE R.cnumber = C.cnumber
  ) AS ST,
  ( SELECT MM.mtitle, ST.stnumber
    FROM MOVIE MM, SCREENING_TABLE SST
    WHERE MM.mpnumber = SST.mpnumber
  ) AS M
WHERE ST.stnumber = M.stnumber
GROUP BY ST.rank
ORDER BY ST.rank ASC;
   

-- 3. 청소년 고객이 선호하는 장르의 분포
SELECT M.mgenre, COUNT(*)
FROM
  ( SELECT I.mpnumber
    FROM
      ( SELECT R.tnumber
        FROM
          ( SELECT cnumber
            FROM CUSTOMER
            WHERE age < 20 ) AS C,
          RESERVATION R
        WHERE
          R.cnumber = C.cnumber
      ) AS T, INFORMATION I
    WHERE I.tnumber = T.tnumber
  ) AS TL, MOVIE M
WHERE M.mpnumber = TL.mpnumber
GROUP BY M.mgenre



-- * 특정 테이블의 특정 Column, 특정 조건에 대해 view 3개 생성하기
-- 1. 특정장르의영화목록
CREATE VIEW
-- 2. 상영중인특정영화의예매가능한시간
CREATE VIEW 
-- 3. 특정감독의작품들의총관객수
CREATE VIEW 

-- * 자유 Query 5개(적용한 기술에 대한 상세 설명 첨부 필요) 어떠한 Query Capture보다 우선시하여 작성할 것.
-- 1. LIMIT_OFFSET(출력 화면을 보기 좋게 다듬는 기능)
-- e.g.) 한 주 동안 상영된 영화를 날짜별로 (그리고 상영관 별로) 출력
-- 2. DATE_TRUNC(월말의 날짜를 표시하는 기능) e.g.) 지난달 결제 목록 중 비회원의 정보 삭제
-- 3. EXTRACT(필요한 속성만 추출하는 기능)
-- e.g.) 상영 시간표의 이 시간대가 조조/심야/일반인지 알려달라
-- 4. WITH(WHERE와 비슷한 기능이나 SQL의 형식의 차이가 있음) e.g.) 특정 배우가 주연을 출연한 영화의 목록
-- 5. LIKE(와일드카드 ‘%’, ’_’를 사용하여 패턴과 텍스트의 값을 일치시키는 기능) e.g.) 이름 중 성씨가 ‘kim’인 고객의 예매 목록







-- 자유로운 쿼리

